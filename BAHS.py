import requests
import sys
import ipcalc
import urllib3
import threading
import argparse
import time
import os
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

parser = argparse.ArgumentParser()
parser.add_argument('-d', type=str)
parser.add_argument('-i', type=str)
parser.add_argument('-s', type=str)
parser.add_argument('-l', type=str)
parser.add_argument('-r', type=str, default=True)
args = parser.parse_args()

domain      = args.d
word        = args.s
mask        = args.i
list_ip     = args.l
red         = args.r

if domain == None or word == None or (mask == None and list_ip == None) or not(mask == None or list_ip == None):
    print('Example:  python index.py -d "example.com" -i "10.0.0.0/18" -s "keyword" -r True\n[-d] domain name\n[-i] ip     or     [-l] ip list file(.txt)\n[-s] keyword\n[-r] boolean(True,False)')
    sys.exit()

if red == "False":
    red = False
elif red == "True":
    red = True

def find_word(src, word_):
    beginPos    = src.find('<title')
    if beginPos == -1:
        return False
    endPos      = src.find('</title>')
    if endPos == -1:
        return False
    title       = src[beginPos+7:endPos]
    if title.find(word_) != -1:
        return True
    else :
        return False

class myThread (threading.Thread):
    def __init__(self, threadID,ip,lock):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.ip = ip
        self.lock = lock
        
    def run(self):
        try:
            r = requests.get('http://'+str(self.ip),headers={'host':domain,'User-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'}, verify=False,timeout=5,allow_redirects=red)
            if r.status_code == 200:
                result = find_word(r.text,word)
                if result == True:
                    lock.acquire()
                    file = open(domain+'.txt','a')
                    file.write(str(self.ip)+'\n')
                    file.close()
                    lock.release()
                    print(str(self.ip), 'detect...')
        except:
            pass
            
        try:
            r = requests.get('https://'+str(self.ip),headers={'host':domain,'User-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'}, verify=False,timeout=5,allow_redirects=red)
            if r.status_code == 200:
                result = find_word(r.text,word)
                if result == True:
                    lock.acquire()
                    file = open(domain+'.txt','a')
                    file.write(str(self.ip)+'\n')
                    file.close()
                    lock.release()
                    print(str(self.ip), 'detect...')
        except:
            pass


clear = lambda: os.system('cls')

lock = threading.Lock()

#time_int = (ip_size/1000)*10
#time_sec = int(time_int) % 60
#time_minut = int(int(time_int) / 60 ) % 60
#time_h = int(int(time_int) / 3600 ) % 24
#time_d = int(int(int(time_int) / 3600 ) / 24)

count = 0
if list_ip == None:
    ip_size = ipcalc.Network(mask).__len__()
    for ip_ in ipcalc.Network(mask):
        count = count + 1
        
        if count % 100 == 0:
            clear()
            ip_size = ip_size - 100
            time_int = (ip_size/1000)*10
            time_sec = int(time_int) % 60
            time_minut = int(int(time_int) / 60 ) % 60
            time_h = int(int(time_int) / 3600 ) % 24
            time_d = int(int(int(time_int) / 3600 ) / 24)
            print("Time\t", end="")
            print('%02d'%time_d,'%02d'%time_h,'%02d'%time_minut,'%02d'%time_sec,sep=':')
            print("IP\t",end="")
            print(ip_)

        if count == 1000:
            clear()
            print("Time\t", end="")
            print('%02d'%time_d,'%02d'%time_h,'%02d'%time_minut,'%02d'%time_sec,sep=':')
            print("IP\t",end="")
            print(ip_)

            count = 0
            time.sleep(10)
        myThread(count,ip_,lock).start()
elif mask == None:
    file_ = open(list_ip,'r')
    lines = file_.readlines()
    ip_size = lines.__len__()
    for line in lines:
        ip_ = line.strip()
        count = count + 1
        
        if count % 100 == 0:
            clear()
            ip_size = ip_size - 100
            time_int = (ip_size/1000)*10
            time_sec = int(time_int) % 60
            time_minut = int(int(time_int) / 60 ) % 60
            time_h = int(int(time_int) / 3600 ) % 24
            time_d = int(int(int(time_int) / 3600 ) / 24)
            print("Time\t", end="")
            print('%02d'%time_d,'%02d'%time_h,'%02d'%time_minut,'%02d'%time_sec,sep=':')
            print("IP\t",end="")
            print(ip_)

        if count == 1000:
            clear()
            print("Time\t", end="")
            print('%02d'%time_d,'%02d'%time_h,'%02d'%time_minut,'%02d'%time_sec,sep=':')
            print("IP\t",end="")
            print(ip_)

            count = 0
            time.sleep(10)
        myThread(count,ip_,lock).start()

        